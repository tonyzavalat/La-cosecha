var pusher=null;function init_pusher()
{try{pusher=new Pusher('70744d379f87a37df68a',{encrypted:true,authEndpoint:'/api/pusher/auth/',auth:{params:{CSRFToken:window.csrftoken},headers:{'X-CSRFToken':window.csrftoken}}});pusher.connection.bind('error',function(err){console.log("Issue connecting to pusher.",err);});}
catch(ex)
{console.log('Exception initiating Pusher');console.log(ex);}}
function init_pusher_channels(log_output){console.log('init_pusher_channels');Pusher.log=function(message){};init_pusher();if(typeof pusher==="undefined"||pusher==null)
{return;}
var user_channel=pusher.subscribe(MARVELAPP.settings.PUSHER.USER_CHANNEL_PREFIX+ MARVELAPP.models.current_user.id);var all_channel=pusher.subscribe("notify");all_channel.bind(MARVELAPP.constants.pusher.EVENT_MESSAGE,function(data){showNotificationMessage(projectConstants.NOTIFY_TYPE_NEWS,data);});user_channel.bind(MARVELAPP.constants.pusher.EVENT_SKETCH,function(data){if(typeof MARVELAPP.models.current_project!=='undefined'){if(MARVELAPP.models.current_project.id==data.content.project_id){setup_selected_layers(false,"",data.content);}}});user_channel.bind(MARVELAPP.constants.pusher.EVENT_TEAM_INVITE,function(data){var invite_data=data.content;var team_id=invite_data.team_id;sync_team_data(team_id)});user_channel.bind(MARVELAPP.constants.pusher.EVENT_TEAM_REMOVAL,function(data){var team_id=data.content.object;MARVELAPP.models.teams.get("categoryAll").remove(team_id);});user_channel.bind(MARVELAPP.constants.pusher.EVENT_TEAM,function(data){});user_channel.bind(MARVELAPP.constants.pusher.EVENT_TEAM_MEMBER_ADDED,function(data){var member=new MARVELAPP.MemberModel(data.content.member);var team=MARVELAPP.models.teams.get('categoryAll').findWhere({id:data.content.team});team.get('users').add(member);});user_channel.bind(MARVELAPP.constants.pusher.EVENT_TEAM_MEMBER_REMOVED,function(data){var team=MARVELAPP.models.teams.get('categoryAll').findWhere({id:data.content.team});var members=team.get('users');var member=members.findWhere({id:data.content.member.id});members.remove(member);});user_channel.bind(MARVELAPP.constants.pusher.EVENT_CUSTOMER_IMPORT,function(data){console.log('import pusher message');$.ajax({type:'GET',url:'/api/v1/import/'+ data.content.provider_name+'/users/',success:function(response){MARVELAPP.models.current_user.trigger('customerImportSuccess',response,data.content.provider_name);},error:function(response){MARVELAPP.models.current_user.trigger('customerImportError',response);},});});user_channel.bind(MARVELAPP.constants.pusher.EVENT_INTERCOM_USER_UPDATED,function(data){if(data.content.object&&Object.size(data.content.object)>0){MARVELAPP.models.current_user.set("intercom",data.content.object);}});function sync_team_data(team_id)
{var team=MARVELAPP.models.teams.get("categoryAll").getModelByTeamId(team_id);var is_delete=false;var is_new=false;if(typeof team==="undefined"){team=new MARVELAPP.TeamModel();team.set({"id":team_id})
is_new=true;}
team.fetchTeam(team_id);if(team.attributes.is_deleted==true){is_delete=true;}
if(is_delete==true){MARVELAPP.models.teams.get("categoryAll").remove(team.id);}
else{if(is_new)
{MARVELAPP.models.teams.get("categoryAll").add(team);console.log("Adding team")}
else
{MARVELAPP.models.teams.get("categoryAll").set([team],{remove:false});console.log("Team set")}}}
user_channel.bind(MARVELAPP.constants.pusher.EVENT_NOTIFICATION,function(data){update_notifications(data.content.actor_id);});user_channel.bind(MARVELAPP.constants.pusher.EVENT_TEAM_PROJECT,function(data){var project_data=data.content.object;var project=MARVELAPP.models.projects.get("project_collection").getModelByProjectId(project_data.id);var team_fk=undefined;var is_new=false;if(typeof project==="undefined"){if(project_data.name.indexOf("Copy of")>-1){project=MARVELAPP.models.projects.get("project_collection").getModelByName(project_data.name);return;}}
if(typeof project==="undefined"){project=new MARVELAPP.ProjectModel();is_new=true;}
else{team_fk=project.get("team_fk");}
var project_id=undefined;if(typeof MARVELAPP.models.current_project!=="undefined"){project_id=MARVELAPP.models.current_project.id;}
if(project_id==project.id&&project_data.images==null){project.set("project_image",project_data.project_image);project.set("name",project_data.name)
project.set("num_images",project_data.num_images)}
else
{project.set(project_data,{remove:false});}
var sync=true;if(project.get("is_deleted")==false){if(team_fk===null&&project.get("owner_fk")!=MARVELAPP.models.current_user.id){MARVELAPP.models.projects.get("project_collection").remove(project.id);}
else{if(project_id!=project.id){if(is_new==true){MARVELAPP.models.projects.get("project_collection").set([project],{remove:false});}
else
{MARVELAPP.models.projects.get("project_collection").add(project);}}
else
{sync=false;}}}
else{MARVELAPP.models.projects.get("project_collection").remove(project.id);}
if(sync==true){MARVELAPP.models.projects.get("project_collection").trigger('sync',MARVELAPP.models.projects.get("project_collection"));}});user_channel.bind(MARVELAPP.constants.pusher.DIRECTORY_ENTRY_ADDED,function(data){var personData=data.content;var person=new MARVELAPP.PersonModel(personData);MARVELAPP.collections.PeopleCollection.add(person);});user_channel.bind(MARVELAPP.constants.pusher.DIRECTORY_ENTRY_REMOVED,function(data){var idsToRemove=data.content.to_remove;_.each(idsToRemove,function(value,key){var person=MARVELAPP.collections.PeopleCollection.findWhere({id:value});MARVELAPP.collections.PeopleCollection.remove(person);});});try{if(typeof pusherProjectChannel!=="undefined"&&typeof MARVELAPP.models.current_project!=="undefined")
{bindPusherProjectChannel();}}
catch(ex)
{console.log(ex);}}
function update_notifications(user_id){if(user_id!=MARVELAPP.models.current_user.id){setTimeout(function(){try{$('header .activity').addClass('unread');MARVELAPP.Router.currentView.renderActivities();}catch(ex){}},5000);}}
function manage_visibility()
{Visibility.change(function(e,state){if(Visibility.hidden())
{console.log("Pusher hidden disconnect");pusher.disconnect();}
else
{console.log("Pusher hidden attach");init_pusher_channels();}});}
init_pusher_channels(false);